//
//  TwitterViewController.h
//  albinia
//
//  Created by Francesco Novelli on 10/02/14.
//  Copyright (c) 2014 runcode. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FHSTwitterEngine.h"
@interface TwitterViewController : UITableViewController <FHSTwitterEngineAccessTokenDelegate>

@end
