//
//  FirstViewController.m
//  albinia
//
//  Created by Francesco Novelli on 10/02/14.
//  Copyright (c) 2014 runcode. All rights reserved.
//

#import "FirstViewController.h"
#import "TFHpple.h"

@interface FirstViewController ()

@end

@implementation FirstViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    NSURL *baseURL = [NSURL URLWithString:@"http://www.cfr.toscana.it/monitoraggio/dettaglio.php?id=TOS03005895&title=Albegna%A0a%A0Marsiliana%20Siap%20-%20Manciano%20(GR)&type=idro"];
    
    NSData  * data      = [NSData dataWithContentsOfURL:baseURL];
    
    TFHpple * doc       = [[TFHpple alloc] initWithHTMLData:data];
    NSArray * elements  = [doc searchWithXPathQuery:@"//*[@id='graph']/img"];
    
    TFHppleElement * element = [elements objectAtIndex:0];
    
    NSLog(@"element %@", [element objectForKey:@"src"]);
    NSString *url = [element objectForKey:@"src"];
    NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:url relativeToURL:baseURL]];
    
    self.imageView.image = [UIImage imageWithData:imageData];
    

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)refresh:(id)sender {
    [self viewDidLoad];
}
@end
