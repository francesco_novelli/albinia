//
//  FirstViewController.h
//  albinia
//
//  Created by Francesco Novelli on 10/02/14.
//  Copyright (c) 2014 runcode. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FirstViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIButton *refreshImage;
- (IBAction)refresh:(id)sender;

@end
